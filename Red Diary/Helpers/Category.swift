//
//  Category.swift
//  Red Diary
//
//  Created by IMCS on 9/2/19.
//  Copyright © 2019 123 Apps Studio LLC. All rights reserved.
//

import Foundation

struct Category {
    var categoryName : String
    var categoryType : CategoryType
    var recurring : Bool
    var frequency : Frequency
    var startDate : Date
    var endDate : Date
    var amount : Double = 0.0

}

enum CategoryType {
    case earning, expense
}

enum Frequency {
    case daily, weekly, biweekly, monthly, bimonthly
}
