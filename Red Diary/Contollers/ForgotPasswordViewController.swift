//
//  ForgotPasswordViewController.swift
//  Red Diary
//
//  Created by IMCS on 9/6/19.
//  Copyright © 2019 123 Apps Studio LLC. All rights reserved.
//

import UIKit
import Firebase
class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var emailAddressTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.alpha = 0
        Utilities.styleFilledButton(forgotPasswordButton)
        

       LoginViewController.backButton?.setValue("Login", forKey: "Title")
    
    }
    
    @IBAction func sendResetLinkAction(_ sender: Any) {
        
        guard let email = emailAddressTextField.text, email != "" else {
            self.showErrorMessage("The email address cannot be empty")
            return
        }
        
        Auth.auth().sendPasswordReset(withEmail: email){ (error) in
            if error == nil {
                self.showErrorMessage("Recovery Email Sent to your email, follow the link to reset your password")
              
                self.errorLabel.textColor = .blue
            } else {
                self.showErrorMessage("Error! Validate your email and try again")
            }
            
        }
        
    }
    
    func showErrorMessage(_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
