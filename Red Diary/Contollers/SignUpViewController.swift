//
//  SignUpViewController.swift
//  Red Diary
//
//  Created by IMCS on 9/1/19.
//  Copyright © 2019 123 Apps Studio LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import Firebase

class SignUpViewController: UIViewController {
    
    
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!

    @IBOutlet weak var startAsGuest: UIButton!
    var ref : DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        ref = Database.database().reference()
            errorLabel.alpha = 0
            Utilities.styleHollowButton(signUpButton)
            Utilities.styleHollowButton(startAsGuest)
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(loginTapped))

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated:true);

    }

    @objc func loginTapped(){
        
        self.navigationController?.pushViewController(Utilities.navigateToSb(sbfileName: "Main", sbIdentifier: "loginSb", vcName: LoginViewController.self), animated: true)
        
    }
    
    func validateFields() -> String?  {
        //Check that all fields are filled in
        
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            return "Please fill in all fields."
        }
        
        
        if Utilities.isPasswordValid(((passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)) == false {
            return "Please make sure your password is atleast 8 characters, contains a number and a special character."
        }
        
        return nil
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        let error = validateFields()
        
        if error != nil {
            showErrorMessage(error!)
        }
        else{
            
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                if err != nil {
                    self.showErrorMessage("Error creating user.")
                }else {
                    self.showErrorMessage("Success")
                    self.errorLabel.textColor = .green
                    
                   
                    self.ref.child("Users").child(result!.user.uid).setValue(["firstname" :      firstName,
                                                                         "lastname" : lastName,
                                                                         "email" : email])
                self.navigationController?.pushViewController(Utilities.navigateToSb(sbfileName: "Dashboard", sbIdentifier: "DashboardSb", vcName: DashboardViewController.self), animated: true)
                }
            }
            
        }
    }
    func showErrorMessage(_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    @IBAction func startAsGuest(_ sender: UIButton) {
        
        Auth.auth().signInAnonymously { (result: AuthDataResult?, error : Error?) in
            
            if let error = error {
                
                print(error.localizedDescription)
                return
            }
            
            print("Sucessfully signed in  anonymously")
            self.ref.child("Users").child(result!.user.uid).setValue(["firstname" : "Anonymous",
                                                                      "lastname" : "Anonymous",
                                                                      "email" : "Anonymous"])
            
           self.navigationController?.pushViewController(Utilities.navigateToSb(sbfileName: "Dashboard", sbIdentifier: "DashboardSb", vcName: DashboardViewController.self), animated: true)
            
        }
        
        
    }
    
}
