//
//  ExpensesAndEarningsTableViewController.swift
//  Red Diary
//
//  Created by IMCS Group on 9/2/19.
//  Copyright © 2019 123 Apps Studio LLC. All rights reserved.
//
import UIKit

class ExpensesAndEarningsTableViewController: UITableViewController {
    @IBOutlet weak var frequencyButton: UIButton!
    @IBOutlet weak var startDateButton: UIButton!
    @IBOutlet weak var endDateButton: UIButton!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var AddBarButton: UIBarButtonItem!
    @IBOutlet weak var expenseEarningTextField: UITextField!
    @IBOutlet weak var recurringSwitch: UISwitch!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var frequencyCell: UITableViewCell!
    @IBOutlet var expenseTableView: UITableView!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    var frequencySelected : String?
    let frequency : [String] = ["Biweekly" , "Weekly" ,"Daily" , "Monthly","Twice a Month"]
    var toolBar = UIToolbar()
    var picker  =  UIPickerView()
    var datePicker = UIDatePicker()
    var buttonTag : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        startDateButton.titleLabel?.numberOfLines = 1
        startDateButton.titleLabel?.adjustsFontSizeToFitWidth = true
        endDateButton.titleLabel?.numberOfLines = 1
        endDateButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    override func viewDidAppear(_ animated: Bool) {
        setUpElements()
    }
    func setUpElements() {
        let font = UIFont.systemFont(ofSize: 25)
        AddBarButton.setTitleTextAttributes([NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : font], for: .normal)
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy"
        let strDate = dateFormatter.string(from: date)
        print(strDate)
        startDateButton.titleLabel?.text = strDate
        endDateButton.titleLabel?.text = strDate
    }
   func createToolBarView() {
    
    toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
    
    toolBar.barTintColor = UIColor(red: 255.0/255.0, green: 51.0/255.0, blue: 00.0/255.0, alpha: 1.0)
    let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onDoneButtonTapped));
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(onCancelButtonTapped));
    toolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    self.view.addSubview(toolBar)
    }
    //Calls this function when the tap is recognized.
    @objc override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func createUiPickerView(){
        picker = UIPickerView.init()
        picker.delegate = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
    }
    @objc func onCancelButtonTapped(){
        toolBar.removeFromSuperview()
        switch buttonTag {
        case 1:
            // frequency
            picker.removeFromSuperview()
            break
        case 2:
            // start Date
            datePicker.removeFromSuperview()
            break
        case 3:
            // End Date
            datePicker.removeFromSuperview()
            break
        default:
            print("Cancel")
        }
    }
    @objc func onDoneButtonTapped() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy"
        toolBar.removeFromSuperview()
        switch buttonTag {
        case 1:
            // Frequency
             picker.removeFromSuperview()
            break
        case 2:
            // Start Date
            let strDate = dateFormatter.string(from: datePicker.date)
            startDateButton.titleLabel?.text = strDate
            datePicker.removeFromSuperview()
            break
        case 3:
            // End Date
            let strDate = dateFormatter.string(from: datePicker.date)
            endDateButton.titleLabel?.text = strDate
            datePicker.removeFromSuperview()
            break
        default:
         print("Done")
        }
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = UIColor.white
        datePicker.setValue(UIColor.black, forKey: "textColor")
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.contentMode = .center
        datePicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(datePicker)
        }
    @IBAction func ButtonTapped(_ sender: UIButton) {
        sender.titleLabel?.numberOfLines = 1
        sender.titleLabel?.adjustsFontSizeToFitWidth = true
        buttonTag = sender.tag
        // frequency Button Tag = 1
        // Start Date Button Tag = 2
        // End Date Button Tag = 3
        switch sender.tag {
        case 1:
        // frequency Button Was Pressed
        createUiPickerView()
        createToolBarView()
        break
        case 2:
        // start Date Button Was pressed
        showDatePicker()
        createToolBarView()
        break
        case 3:
        // End Date Button was Pressed
        showDatePicker()
        createToolBarView()
        break
        default:
            print("No button was pressed ")
        }
    }
  
    @IBAction func segmentControlTapped(_ sender: UISegmentedControl) {
      
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
           // expense is pressed`
            expenseEarningTextField.attributedPlaceholder = NSAttributedString(string: "Expense Name",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
            break
        case 1:
            // earning is pressed
            expenseEarningTextField.attributedPlaceholder = NSAttributedString(string: "Earning  Name",
                                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
            break
        default:
            break
        }
        
    }
    
  
    
    @IBAction func recurringSwitchChanged(_ sender: UISwitch) {
        if recurringSwitch.isOn {
            frequencyLabel.isHidden = false
            frequencyButton.isHidden = false
            endDateButton.isHidden = false
            endDateLabel.isHidden = false
            startDateLabel.text = "Start Date"
            
            DispatchQueue.main.async {
                self.expenseTableView.reloadData()
            }
        }
        else {
            frequencyLabel.isHidden = true
            frequencyButton.isHidden = true
            endDateButton.isHidden = true
            endDateLabel.isHidden = true
            startDateLabel.text = "Date"
            DispatchQueue.main.async {
        
                self.expenseTableView.reloadData()
            }

        }
    }

        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            if indexPath.row == 3 && recurringSwitch.isOn == false
            {
                return 0.0
            }
            if indexPath.row == 5 && recurringSwitch.isOn == false
            {
                return 0.0
            }
        return 44.0

    }


}
extension ExpensesAndEarningsTableViewController : UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return frequency.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return frequency[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        frequencySelected = frequency[row]
        frequencyButton.titleLabel?.text = frequencySelected
    }
    
}

