//
//  ViewController.swift
//  Red Diary
//
//  Created by Pandu on 2/27/19.
//  Copyright © 2019 123 Apps Studio LLC. All rights reserved.
//
import UIKit
import Firebase
import FirebaseAuth
class LoginViewController: UIViewController {
    
    static var backButton : UIBarButtonItem?
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        errorLabel.alpha = 0
        Utilities.styleHollowButton(loginButton)
        Utilities.styleHollowButton(forgotPasswordButton)
        self.navigationItem.setHidesBackButton(true, animated:true);
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "SignUp", style: .plain, target: self, action: #selector(SignUpTapped))
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    @objc func SignUpTapped() {
             self.navigationController?.pushViewController(Utilities.navigateToSb(sbfileName: "Main", sbIdentifier: "signUpSb", vcName: SignUpViewController.self), animated: true)
        }
    @IBAction func loginTapped(_ sender: Any) {
        let email = emailAddressTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        Auth.auth().signIn(withEmail: email, password: password) {
            (result, error) in
            if error != nil {
                self.showMessage("We couldn't locate the account associated with this email, please try again or create an account.")
                self.errorLabel.textColor = .red
            }
            else {
                self.showMessage("Success")
                self.errorLabel.textColor = .green
                self.navigationController?.pushViewController(Utilities.navigateToSb(sbfileName: "Dashboard", sbIdentifier: "DashboardSb", vcName: DashboardViewController.self), animated: true)
            }
        }
    }
    func showMessage(_ message: String) {
        self.errorLabel.text = message
        self.errorLabel.alpha = 1
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

